##### Curso introductorio a PHP+Laravel
---

# Servidor Web

Iniciar un servicio web con php

```bash
php -S localhost:5001
```

Salida del comando

```
PHP <versión de php> Development Server started at <fecha actual>
Listening on http://localhost:5001
Document root is <directorio actual>
Press Ctrl-C to quit.
```

Podemos acceder al servidor haciendo click en el enlace http://localhost:5001

>La palabra *localhost* hace referencia a la máquina local. El número que sigue a los dos puntos es un número de puerto que podemos elegir entre 1024 y 65535. El puerto no debe estar en uso por otra aplicación. 






