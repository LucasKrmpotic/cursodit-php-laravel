<?php
    $ch = curl_init("https://www.boredapi.com/api/activity");
    
    $headers = array(
        "Content-Type: application/json"
    );
    curl_setopt($ch, CURLOPT_CUSTOMREQUEST, "GET");
    curl_setopt($ch, CURLOPT_HTTPHEADER, $headers);
    
    $response = curl_exec($ch) ;
    $info = curl_getinfo($ch);
        
    $data = json_decode($response, true);
    echo $data;
?>

