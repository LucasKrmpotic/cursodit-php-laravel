# Cursos DIT - Introducción a Laravel y Php

* [Guía de instalación](./instalacion/Instalacion-de-Composer-y-de-Laravel.pdf)

* 1era clase
    * [Filminas teoría](./filminas/Primera-Clase.pptx)  
    * [Apunte práctica (incompleto)](./clase1/README.md)

* 2da clase
    * [Filminas teoría](./filminas/Segunda-Clase.pptx)  
    * [Apunte práctica (completísimo)](./clase2/README.md)

* 3ra clase
    * [Apunte práctica (completísimo tambien)](./clase3/README.md)

* 4ta clase
    * Pronto...