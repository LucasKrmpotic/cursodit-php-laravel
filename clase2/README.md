# Arrancando con Laravel

## Crear una nueva aplicación

La creación de una nueva aplicación Laravel se resuelve con el comando: 

```
laravel new nombre-de-la-aplicacion
```

Luego de ello podemos correr la aplicación ingresando a la carpeta del proyecto y lanzando el servidor de artisan

```
cd nombre-de-la-aplicacion
php artisan serve
```

Hecho esto tendremos la aplicación disponible en [http://localhost:8000](http://localhost:8000)

> **Nota:** si se está usando un servidor apache o nginx no será necesario lanzar le servidor de artisan, pero en ese caso revisar que el proyecto se cree en la carpeta publica del servidor (```xampp/htdocs```, ```var/www/html```, o la que corresponda. Si es así, la aplicación estará disponible en http://localhost 

Si todo salió bien veremos la pantalla de bienvenida de Laravel. Esta pantalla está definida en el archivo ```resources/views/welcome.blade.php```

> Blade es el motor de plantillas de Laravel, por lo tanto todos los archivos de vistas terminarán con ```.blade.php``` además de definirse en el directorio ```resources/views/``` 

## Introducción a las rutas 
Todos los archivos que definen comportamiento de ruteo los encontraremos en el directorio ```routes```. En este curso solo usaremos el archivo ```routes/web.php```

> En el archivo ```api.php``` se definen rutas de api. En el archivo ```channels.php``` se definen websockets y el archivo ```console.php``` nos ayuda a definir comandos personalizados de Artisan. 

Cuando se crea un nuevo proyecto, Laravel nos creará la ruta:

```php
Route::get('/', function () {
    return view('welcome');
});
```

Esta sentencia define que las peticiones ```get``` a la ruta ```/``` ejecutarán una función que lo que hace es servir la plantilla ```resources/views/welcome.blade.php```. 

Si queremos definir una nueva ruta podemos hacer: 

```php
Route::get('/nuevaruta', function () {
    return view('plantilla_para_nueva_ruta');
});
```

Por supuesto será necesario crear el archivo ```resources/views/plantilla_para_nueva_ruta.blade.php```.  

Desde ya que el router no solo es capaz de servir templates. La función que se ejecuta cuando la ruta es llamada puede hacer mucho más que eso. En el apartado siguiente veremos como se pueden definir ruteos para ejecutar funciones definidas en un controlador. 

## Creación del modulo de autenticación

Para crear todo el módulo de autenticación, esto es login, register, logout y recuperación de contraseña, solo necesitamos ejecutar el comando:

```
php artisan make:auth
```

Luego de ello veremos que artisan nos habrá generado un conjunto de vistas en la carpeta ```resources/views/auth/```, además de la vista ```home.blade.php```, el controlador de Home, el modelo User, y varios controladores en el directorio ```app/Http/Controllers/Auth/```  entre otros archivos.

En próximos encuentros del curso ahondaremos en el módulo de autenticación. 

Por lo pronto nos interesa señalar que con el comando anterior artisan ha generado también las rutas de la aplicación para el módulo de usuarios. Esto puede verse en el archivo ```routes/web.php``` en las sentencias: 

```php
Auth::routes();
Route::get('/home', 'HomeController@index')->name('home');
```

En el primer caso se ha incluido un conjunto de rutas definidas en la clase Auth. Esta forma de agrupar definiciones de rutas la veremos mas adelante. 

Por el momento nos interesa poner atención en la línea 

```php
Route::get('/home', 'HomeController@index')->name('home');
```

Esta línea se está estableciendo que al recibir una petición ```get``` a la ruta ```/home``` se ejecutará la función ```index``` del controlador ```HomeController```. En los apartados siguientes definiremos muchas rutas de este modo usando la sintáxis ```NombreControlador@nombre_funcion```.

El ```->name('home')``` establece un nombre interno para la ruta, para que pueda ser llamada desde otros controladores. 

## Creación del modelo y el controlador de Artistas

Con el comando: 

```
php artisan make:model Artista -mc
```

creamos el modelo y el controlador del módulo de artistas. Luego de la ejecujción de este comando tendremos creados los archivos ```app/Artista.php``` y ```app/Http/Controller/ArtistaController.php```. En los próximos apartados se trabajará con estos pdos archivos más el archivo de rutas ```routes/web.php```

## El modelo Artistas
El modelo es la capa de acceso a datos en la que además podemos definir funciones de procesamiento de esos datos, tanto al momento de almacenarlos como de buscarlos y recuperarlos. 

Para operaciones de busqueda y recuperación de datos, 
el controlador solo debe pedirle al modelo el conjunto especifico de datos y despreocuparse de si esos datos están en una base de datos, en una api externa o en un archivo de texto. 

Para operaciones de salvado de datos, el controlador solo entrega los datos al modelo y es éste quien debe determinar si son datos válidos o no, como se guardan, si están completos, si es preciso hacer alguna transformación previa, etc. 

Por ello es normal que los modelos tengan operaciones para altas-bajas-modificación, listados (con filtros), interacción con otros modelos.

En nuestro caso el modelo Artistas tiene las siguientes funciones:

### Buscar por id: 

```php
public function findById($id){
    return array_filter($this->artistas, function ($k) use ($id) {
        return $k['id'] == $id;
    });
} 
```

### Buscar por apellido:

```php
public function findByLastName($lastName){
    return array_filter($this->artistas, function ($k) use ($lastName) {
        return $k['apellido'] == $lastName;
    });         
}
```

### Guardar:

```php
public function guardar($artista){

    if (!$this->findByLastName($artista['apellido'])) {
        $artista_nuevo = array_merge(
            ["id" => strval(count($this->artistas) + 1)], 
            $artista
        );
        array_push($this->artistas, $artista_nuevo);
        return $artista_nuevo;
    }
    return false;
}
```

### Buscar artistas en Spotify en base a un término de búsqueda:

Definimos los atributos api_url y token como constantes del modelo:
```php 
    const spotifyUrl = "https://api.spotify.com/v1/";
    const spotifyToken = "aca va tu token";
```

```php
public static function getSpotyArtists($termino) {
    
    $ch = curl_init(Artista::spotifyUrl . "search?query=" . $termino . "&type=artist&offset=0&limit=20");
    
    $headers = [
        'Authorization: Bearer '.Artista::spotifyToken,
        'Content-Type: application/json',
        'Accept: application/json'
    ];
    curl_setopt($ch, CURLOPT_CUSTOMREQUEST, "GET");
    curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
    curl_setopt($ch, CURLOPT_HTTPHEADER, $headers);

    $response = curl_exec($ch) ;
    curl_close($ch);
    return json_decode($response, true);
}
```

### Para obtener los albumes de un artista

```php
public static function getAlbumesByArtist($idArtista) {

    $ch = curl_init(Artista::spotifyUrl . "artists/" . $idArtista . "/albums");
    
    $headers = [
        'Authorization: Bearer '.Artista::spotifyToken,
        'Content-Type: application/json',
        'Accept: application/json'
    ];
    curl_setopt($ch, CURLOPT_CUSTOMREQUEST, "GET");
    curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
    curl_setopt($ch, CURLOPT_HTTPHEADER, $headers);

    $response = curl_exec($ch) ;
    $info = curl_getinfo($ch);
    curl_close($ch);
    return json_decode($response, true);
}
```


## El controlador Artistas

Uso de las funciones del modelo

```php
public function list(Request $request) {
    $Artista = new Artista;
    return $Artista->findAll();
}
```

```php
public function view (Request $request, $id) {
    $Artista = new Artista;
    if ($artista = $Artista->findById($id)){
            return $artista;
        }
        return "no se encontró el artista"; 
}
```

```php
public function create (Request $request, $data) {
    $artista = json_decode($data, true);
    $Artista = new Artista;
    if ($artista_nuevo = $Artista->guardar($artista)) {
            return $artista_nuevo;
    }
    return "El artista ya se encuentra registrado";
}
```

```php
public function getSpotyArtists ($termino) {
    $artistas = Artista::getSpotyArtists($termino);
    try {
        $arts = [];
        foreach ($artistas['artists']['items'] as $artista) {
            array_push($arts, array( 
                'id' => $artista["id"],
                'nombre' => $artista["name"],
                'popularidad' => $artista["popularity"],
                'url' => $artista["external_urls"]["spotify"]
                ) 
            );
        }
        return json_encode($arts);
    } catch (\Throwable $th) {
        return "La búsqueda no obtuvo resultados";
    }
}
```
## Actualización de las rutas

```php
Route::get('/artistas', 'ArtistaController@list')->name('artistas');
Route::get('/artistas/view/{id}', 'ArtistaController@view')->name('artista_view');
Route::get('/artistas/create/{artista}','ArtistaController@create')->name('artista_create');
Route::get('/artistas/{termino}', 'ArtistaController@getSpotyArtists')->name('artistas_spotify');
```

