<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', function () {
    return view('welcome');
});

Route::get('/nuevaruta', function () {
    return view('plantilla_para_nuevaruta');
});

Auth::routes();
Route::get('/home', 'HomeController@index')->name('home');

Route::get('/artistas', 'ArtistaController@list')->name('artistas');
Route::get('/artistas/view/{id}', 'ArtistaController@view')->name('artista_view');
Route::get('/artistas/create/{artista}','ArtistaController@create')->name('artista_create');
Route::get('/artistas/{termino}', 'ArtistaController@getSpotyArtists')->name('artistas_spotify');
Route::get('/artista/{id}/albumes', 'ArtistaController@getAlbumesByArtist')->name('artista_albumes');
