<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Artista;

class ArtistaController extends Controller
{
    /**
     * Listado de artistas
     */
    public function list(Request $request) {
        $Artista = new Artista;
        $artistas = $Artista->findAll();
        return view('artistas', ['artistas' => $artistas]);
    }
    /**
     * Obtenemos un artista por su id
     */
    public function view (Request $request, $id) {
        $Artista = new Artista;
        if ($artista = $Artista->findById($id)){
             return $artista;
         }
         return "no se encontró el artista"; 
    }


    /**
     * Creamos un nuevo artista
     */
    public function create (Request $request, $data) {
        $artista = json_decode($data, true);
        $Artista = new Artista;
        if ($artista_nuevo = $Artista->guardar($artista)) {
             return $artista_nuevo;
        }
        return "El artista ya se encuentra registrado";
    }

    public function getSpotyArtists($termino) {
        
        $artistas = Artista::getSpotyArtists($termino);
        try {
            $arts = [];
            foreach ($artistas['artists']['items'] as $artista) {
                array_push($arts, array( 
                    'id' => $artista["id"],
                    'nombre' => $artista["name"],
                    'popularidad' => $artista["popularity"],
                    'url' => $artista["external_urls"]["spotify"]
                    ) 
                );
            }
            return view('artistas', ['artistas' => $arts]);
            
        } catch (\Throwable $th) {
            return "La búsqueda no obtuvo resultados";
        }

    }

    public function getAlbumesByArtist($id) {
        $albumes = Artista::getAlbumesByArtist($id);
        return view('albumes', ['albumes' => $albumes]);
    }
}
