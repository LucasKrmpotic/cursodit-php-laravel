<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Artista extends Model
{
    // Declaramos una variable de instancia y la inicializamos con algunos datos
    private $artistas = [
        [
            'id' => '1',
            'nombre' => 'Ricardo',
            'apellido' => 'Arjona'
        
        ],
        [
            'id' => '2',
            'nombre' => 'Ricardo',
            'apellido' => 'Montaner'
        
        ]
    ];

    // url base de la api
    const spotifyUrl = "https://api.spotify.com/v1/";
    const spotifyToken = "BQC5oMHTQ5wRXfeOHuweQLHOve82RbMOCmKNrkcE_jHyNaH5736Gc5VKGLLc_cacWu9y4WiT5X_mIkzjmetfRyPZuijeFFZvoAgAGAexL6EldRyd7EpB6_0OEz0xPhvGQLn9KQwiEE3PZhivPJuWOKZhhmX2c94s-xuEFw";


    // retorna el contenido de la variable artistas
    public function findAll() {
        return $this->artistas;
    }

    // busca un artista por id
    public function findById($id){
        /**
         * array_filter es una función de php
         * la clausula use ($id) es necesaria para que el parámetro $id
         * sea visible dentro de la función de filtrado 
         */
        return array_filter($this->artistas, function ($k) use ($id) {
            return $k['id'] == $id;
        });         
    }

    public function findByLastName($lastName){
        return array_filter($this->artistas, function ($k) use ($lastName) {
            return $k['apellido'] == $lastName;
        });         
    }

    public function guardar($artista){

        if (!$this->findByLastName($artista['apellido'])) {
            /**
             * usamos array_merge para concatenar 2 arrays
             */
            $artista_nuevo = array_merge(
                ["id" => strval(count($this->artistas) + 1)], 
                $artista
            );
            /**
             * Con array_push agregamos el artista nuevo al array
             */
            array_push($this->artistas, $artista_nuevo);
            return $artista_nuevo;
        }
        return false;
    }

    /**
     * Función para obtener un listado de artistas de Spotify según 
     * un término de búsqueda
     */
    public static function getSpotyArtists($termino) {
        

        // url para solicitar la búsqueda de artistas en base al término - devuelve a lo sumo 20 resultados
        $ch = curl_init(Artista::spotifyUrl . "search?query=" . $termino . "&type=artist&offset=0&limit=20");
        
        //  definimos los headers
        $headers = [
            'Authorization: Bearer '.Artista::spotifyToken,
            'Content-Type: application/json',
            'Accept: application/json'
        ];
        // seteamos los headers
        curl_setopt($ch, CURLOPT_CUSTOMREQUEST, "GET");
        curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
        curl_setopt($ch, CURLOPT_HTTPHEADER, $headers);

        // retornamos la respuesta
        $response = curl_exec($ch) ;
        curl_close($ch);
        return json_decode($response, true);
    }

    /**
     * Función para obtener los albumes de un artista
     * 
     */

    public static function getAlbumesByArtist($idArtista) {
        // url para solicitar la búsqueda de artistas en base al término - devuelve a lo sumo 20 resultados
        $ch = curl_init(Artista::spotifyUrl . "artists/" . $idArtista . "/albums");
        
        //  definimos los headers
        $headers = [
            'Authorization: Bearer '.Artista::spotifyToken,
            'Content-Type: application/json',
            'Accept: application/json'
        ];
        // seteamos los headers
        curl_setopt($ch, CURLOPT_CUSTOMREQUEST, "GET");
        curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
        curl_setopt($ch, CURLOPT_HTTPHEADER, $headers);

        // retornamos la respuesta
        $response = curl_exec($ch) ;
        $info = curl_getinfo($ch);
        curl_close($ch);
        return json_decode($response, true);
    }

}
