@extends('base.base')

@section('contenido')

<table class="table">
    <thead>
        <th>id</th>
        <th>nombre</th>
        <th>apellido</th>
        <th>Buscar artistas</th>
    </thead>
    <tbody>
        @foreach($artistas as $artista)
            <tr>
                <td>{{ $artista['id'] }}</td>
                <td>{{ $artista['nombre'] }}</td>
                <td>{{ $artista['apellido'] }}</td>
                <td><a class="btn btn-xs btn-primary" href="/artistas/{{ $artista['id'] }}/albumes">Albumes</a></td>
            </tr>
        @endforeach       
    </tbody>
</table>

@endsection
