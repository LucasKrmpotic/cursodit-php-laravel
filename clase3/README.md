# Un poco mas de laravel

## Migramos los modelos a la base de datos

La migracion de Laravel se resuelve con el comando: 

```
php artisan migrate
```

Luego de esto, podemos ir al index de nuestra aplicacion y registrar un nuevo usuario.

> **Nota:** si se está usando un servidor apache o nginx no será necesario lanzar le servidor de artisan, pero en ese caso revisar que el proyecto se cree en la carpeta publica del servidor (```xampp/htdocs```, ```var/www/html```, o la que corresponda. Si es así, la aplicación estará disponible en http://localhost 

Si todo salió bien veremos la pantalla de bienvenida de Laravel. Esta pantalla está definida en el archivo ```resources/views/home.blade.php```

Si nos fijamos dentro del directorio ```resources/views/``` veremos que tenemos un archivo ```index.blade.php``` (Que es el nuevo archivo que queremos mostrar al estar logueado el usuario). Para lograr esto debemos modificar la redireccion que nos hace automaticamente el scaffold de laravel, lo que se hace modificando los siguientes archivos:

1.  En el archivo ```app/Http/Controllers/Auth/LoginController.php``` debemos modificar la siguiente linea para que quede de la siguiente manera:
    ```php
     /**
     * Where to redirect users after login.
     *
     * @var string
     */
    protected $redirectTo = '/';
    ```
2. En el archivo ```app/Http/Middleware/RedirectIfAuthenticated.php``` debemos modificar el siguiente metodo para que tambien redirija a ```/```:
    ```php
    /**
     * Handle an incoming request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \Closure  $next
     * @param  string|null  $guard
     * @return mixed
     */
    public function handle($request, Closure $next, $guard = null)
    {
        if (Auth::guard($guard)->check()) {
            return redirect('/');
        }

        return $next($request);
    }
    ```
3. Y finalmente, en el archivo de rutas en ```routes/web.php``` modificamos la ruta que devuelve ```/``` para que renderice la vista que esta en el archivo ```index.blade.php```:
    ```php
    Route::get('/', function () {
        return view('index');
    });
    ```

## Limitar el acceso a las vistas para los usuarios que tienen la sesion iniciada. 

Para nuestro sistema, no vamos a querer que se pueda acceder a la funcionalidad de busqueda en spotify sin tener una cuenta y estar con la sesion iniciada, para lo que vamos a aprovechar algo que Laravel ya nos trae resuelto dentro de una nueva herramienta: Los middlewares.

> Importante abrir el archivo de rutas para que lo siguiente se pueda entender lo mejor posible.

Conceptualmente, un usuario que no este logueado solo deberia poder ver el login y la vista de registracion, por lo que no vamos a tener ningun problema con las vistas que estan dentro de ```Auth::routes();```. Para que el resto verifiquen primero si el usuario esta logueado, las vamos a meter en un grupo que utilice el middleware de autentificacion basico:

```php
Route::group(['middleware' => ['auth']], function() {
    Route::get('/', function () {
        return view('index');
    }); 
    Route::get('/artistas', 'ArtistaController@list')->name('artistas');
    Route::get('/artistas/view/{id}', 'ArtistaController@view')->name('artista_view');
    Route::get('/artistas/create/{artista}','ArtistaController@create')->name('artista_create');
    Route::get('/artistas/{termino}', 'ArtistaController@getSpotyArtists')->name('artistas_spotify');
    Route::get('/artista/{id}/albumes', 'ArtistaController@getAlbumesByArtist')->name('artista_albumes');
});
```

Teniendo hecho esto, si uno intentara acceder a cualquiera de esas rutas, laravel se va a dar cuenta que de no estamos logueados y nos va a mandar al login para que lo hagamos (Esto viene configurado por defecto en el archivo: ```app/Http/Middleware/Authenticate.php```, dentro del metodo llamado ```redirectTo()```). 

En caso de que quisieramos redirigir al usuario a una vista de error o algun otro lugar en lugar de redirigirlo al login, debemos aclarar la ruta en ese archivo y escribirla afuera del grupo que acabamos de definir.

## Definicion de templates y herencia

Como ya les mencionamos antes, los templates de las vistas se definen en la carpeta ```resources/views```, por lo que cada una de nuestras vistas deberia encontrarse ahi dentro.

> **Nota:** Cuando en algun controlador (O en alguna ruta) le decimos a laravel que devuelve una vista mediante la sentencia ```return view(<vista>)``` le estamos indicando que busque ahi adentro el template que queremos que devuelva al cliente. Es importante tener presente que, si quisieramos ordenar nuestros templates en carpetas, para decirle que tiene que renderizar alguno que este dentro de una carpeta hay que aclarar la ruta completa al template separada por puntos. Por ejemplo, para devolver el template que esta dentro de ```resources/views/administracion/recepciones/pendientes.blade.php``` se tendra que escribir  ```return view('administracion.recepciones.pendientes');```

Ahora bien, como ya les habiamos dicho, historicamente si uno queria definir una X cantidad de templates dentro de un sistema web era necesario definir en cada uno de ellos las cabeceras html y el cuerpo de los mismos (Por mas que en todos tuvieramos elementos repetidos, como lo puede ser una sidebar o una navbar). Para darnos una mano con esto, el motor de plantillas blade nos provee de la herencia de templates, en donde nos es posible decirle a un template que hereda de otro y solo define una parte de el. El ejemplo mas tipico es:

* Un template base que contiene todo el contenido de html, la cabecera y los elementos comunes a todo el sistema. En nuestro proyecto, si vamos al template que se encuentra dentro de ```resources/views/base/base.blade.php``` veremos justamente eso.
* Dentro de nuestro template base definimos secciones que queremos que se puedan editar en otros templates a quienes le heredemos nuestro contenido. Esto se logra con la sentencia ```@yield(<nombre>)```, en donde el nombre es el mismo que vamos a usar en donde definamos ese contenido.
* Definir un template que hereda de alguno de nuestro base y define algun contenido (Esto ultimo no es necesario, podemos simplemente fijar la base de nuestros templates en un archivo base, extenderla desde un index que no tiene nada y luego ponerle contenido en los demas templates). Esto se logra, desde el nuevo template, poniendo como primera sentencia ```@extends(<nombre>)``` (Donde el nombre es la ruta completa del template que nos hereda su contenido). En nuestro proyecto podemos ver esto en el template ```resources/views/index.blade.php```.

    > **Nota:** Seria totalmente valido si, en nuestro ejemplo, el template index solo tuviera el ```@extends('base.base')``` y el template ```resources/views/artistas/artistas.blade.php``` heredara de index con ```@extends('index')```
* Rellenar las secciones que definimos como editables en los templates de los que heredamos. Por ejemplo, en nuestro template ```base.blade.php``` definimos una seccion con ```@yield('contenido')```, ahora podriamos rellenar ese espacio en ```artistas.blade.php``` con:

```php
@section('contenido')
    // Algun codigo html aca
@endsection
```

Con esto, tendriamos un template base en el que se encuentra la estructura basica de los HTML de nuestro proyecto. Y con la herramienta de herencia de plantillas no hace falta mas que definir lo que cambia en cada uno de ellos mediante las secciones.

## Incluir bootstrap y JQuery en nuestro proyecto

Tanto los estilos de nuestro proyecto como las librerias de javascript y los demas posibles "assets" que podamos llegar a tener (Imagenes, audio, etc), van en la carpeta ```public``` del proyecto.
> **Nota:** Si bien tenemos otra carpeta para los assets dentro de ```resources```, hay que tener en cuenta que para el contenido que queramos que sea accesible por el navegador solo debemos disponer de la carpeta ```public```.

Dentro de la misma tenemos dos directorios por defecto:
* ```public/css```: En donde pondremos las hojas de estilos css que tengamos.
* ```public/js```: En donde pondremos los archivos de scripts que tengamos.

Luego, desde nuestros templates llamaremos a dichos archivos mediante la sentencia ```{{ asset('<asset>') }}```. Por ejemplo, para llamar al archivo ```app.css``` seria ```{{ asset('css/app.css') }}```. Para ver un ejemplo real de como lo deberiamos escibir tanto para estilos como para scripts podemos consultar el archivo ```resources/views/base/base.blade.php```.

Para incluir bootstrap y JQuery (O cualquier otro estilo/script que queramos), tenemos dos opciones:
1. Bajar los archivos necesarios a esas carpetas y llamarlos desde el codigo como ya se explico arriba.
2. Buscar la tira de HTML que lo trae de internet cada vez que se carga el template.

Por ejemplo, para incluir bootstrap 3 mediante la ultima opcion, seria:
```html
<link href="//netdna.bootstrapcdn.com/bootstrap/3.0.3/css/bootstrap.min.css" rel="stylesheet">
``` 
Y para agregarlo mediante la primera opcion deberiamos bajar los archivos de las paginas de bootstrap y de jquery y ubicarlos apropiadamente en las carpetas que les mencionamos mas arriba.

> **Nota:** Nosotros ya les dejamos incluido tanto bootstrap como JQuery en el template base del proyecto de la clase 3.

> **IMPORTANTE:** Como puede verse en el template ```base.blade.php``` los estilos se los suele cargar en el header del HTML mientras que los scripts se los suele cargar al final del body. Si bien es totalmente valido cargar todo el el header del html, la razon de esta convencion es el hecho de que la visual de la pagina se carga mas rapido de esta manera (Ya que los scripts, que suelen ser mas pesados que los estilos, se cargan cuando el html esta renderizado con el "maquillaje" y todo).

## Documentacion
* HTML:
    * En ingles: https://www.w3schools.com/html/
    * En español: https://www.uv.es/jac/guia/
* Blade:
    * Documentacion oficial de Laravel (En ingles): https://laravel.com/docs/5.8/blade
