<?php

use Faker\Generator as Faker;
use App\Album;

$factory->define(Album::class, function (Faker $faker) {
    return [
        'id_artista' => $faker->word(),
        'nombre' => $faker->word(),
        'url_spotify' => $faker->word()
    ];
});
