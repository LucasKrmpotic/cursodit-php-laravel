<?php

use Faker\Generator as Faker;
use App\Artista;

$factory->define(Artista::class, function (Faker $faker) {
    return [
        'nombre' => $faker->word(),
        'genero' => $faker->word(),
        'popularidad' => $faker->word(),
        'url_spotify' => $faker->word(),
        'id_spotify' => $faker->word()
    ];
});
