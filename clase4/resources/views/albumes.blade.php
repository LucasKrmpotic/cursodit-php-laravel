@extends('base.base')

@section('contenido')

<table class="table">
    <thead>
        <th>id_artista</th>
        <th>nombre</th>
        <th>url_spotify</th>
    </thead>
    <tbody>
        @foreach($albumes as $album)
            <tr>
                <td>{{ $album['id_artista'] }}</td>
                <td>{{ $album['nombre'] }}</td>
                <td>{{ $album['url_spotify'] }}</td>
            </tr>
        @endforeach       
    </tbody>
</table>

@endsection