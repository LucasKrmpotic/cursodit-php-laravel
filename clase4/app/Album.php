<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

use App\Artista;


class Album extends Model
{
    public $timestamps = false;
    protected $table = '';
    protected $fillable = [
        'id_artista',
        'nombre',
        'url_spotify'
    ]

    /**
     * Relacion con el artista.
     * 
     * Devuelve el artista de este album.
     * 
     * @return collection App\Artista
     */
    public function artista() {
        return $this->belongsTo(Artista::class, 'id_artista', 'id');
    }
}
